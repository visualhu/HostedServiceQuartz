
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Quartz;

namespace DemoApp
{
public class QuartzService:IHostedService{
    private readonly ILogger _logger;
    private readonly IScheduler _scheduler;

    public QuartzService(ILogger<QuartzService> logger,IScheduler scheduler){
        _logger=logger;
        _scheduler=scheduler;
    }

    public async Task StartAsync(CancellationToken cancellationToken){
        _logger.LogInformation("Start Quartz Schedule...");
        await _scheduler.Start(cancellationToken);
    }

    public async Task StopAsync(CancellationToken cancellationToken){
        _logger.LogInformation("Stop Quartz Schedule...");
        await _scheduler.Shutdown(cancellationToken);
    }



}
}